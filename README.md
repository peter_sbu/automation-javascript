# Automation-Javascript
- This project demonstrates a program written to execute a test case suite that would normally 
  be done manually. This shows the possibility to reduce manual testing, save time and cost when testing web applications.
- This program provides an end-to-end testing on the magento.softwaretestingboard.com web app 

*Test cases covered (some)*: 

- Navigating to the website and logging in
- Go to a store category/department 
- List items
- Validate error messages
- See the lumaWebsiteTests.cy file for all tests

*Tech Stack*:

- Cypress web testing tool
- Javascript 
- npm 
- node.js

*Setup*:

- Install cypress: goto https://docs.cypress.io/guides/getting-started/installing-cypress
- Create a demo account on https://magento.softwaretestingboard.com/ 
- Insert login credentials into the file loginTestData.js in this project

*Running Cypress*

- Open the project in your IDE
- Open terminal 
- Type: npx cypress open
- Select E2E Testing on the window that appears
- Select your desired browser on the next window
- For any errors goto https://docs.cypress.io/guides/getting-started/opening-the-app 