export class HomePageElements {
    url = 'https://magento.softwaretestingboard.com/';
    signInLink = '.authorization-link';
    successLogin = '.logged-in';
    womenDepartment = '#ui-id-4';
    topsCategory = '#ui-id-9';
    menuDropDownButton = '.switch';
    ordersAndReturns = 'a[href*="guest/form"]';
}