export class WomenTopsPageElements {
    pageTitle = '.base';
    listView = '#mode-list';
    listLimiter = '.limiter-options';
    totalDisplayed = '.toolbar-amount';
    listSorter = '.sorter-options';
    priceRange = 'a[href*="price=40-50"]';
    productPrice = '.price';
    nextPage = '.pages-item-next';
    nextPageTotal = '.toolbar-number';

}
