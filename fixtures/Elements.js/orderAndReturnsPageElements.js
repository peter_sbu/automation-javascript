export class OrdersAndReturnsPageElements {
    orderIDTextbox = '#oar-order-id';
    billingLastNameTextbox = '#oar-billing-lastname';
    findOrderBySelection = '#quick-search-type-id';
    emailOption = 'email';
    emailTextbox = '#oar_email';
    continueButton = '.submit';
    errorMessageElement = '.error.message';
}