import { WomenTopsPageElements } from "../../../fixtures/Elements.js/womenTopsPageElements";
import { WomenTopsPageTestData } from "../../../fixtures/TestData/womenTopsPageTestData";

const womenTopsPageElements = new WomenTopsPageElements();
const womenTopsPageTestData = new WomenTopsPageTestData();

export const showItemsInListMode = () => {
    cy.wait(2000);
    cy.get(womenTopsPageElements.listView).click();
    cy.wait(2000);
    cy.get(womenTopsPageElements.listLimiter).last().select(womenTopsPageTestData.totalItems);
}

export const verifyAscendingOrder = () => {
    const price = [];
   cy.get(womenTopsPageElements.productPrice).each(($priceValue) => price.push($priceValue.text().substring(1)))
     .then(() => { price.join(', ');
     let uniquePriceValue = [...new Set(price)];
     
       cy.expect(parseFloat(uniquePriceValue[0])).to.be.lessThan(parseFloat(uniquePriceValue[1]));
    });
} 

export const confirmTotalItems = () => {
    cy.get(womenTopsPageElements.nextPageTotal).should(($total) => {
        const pageTwoTotal = $total.text().substring(4, 6);
        
        cy.expect((Number(pageTwoTotal)) - Number(womenTopsPageTestData.totalItems)).to.deep.equal(25);
    });
}