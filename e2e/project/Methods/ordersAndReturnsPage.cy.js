import { OrdersAndReturnsPageElements } from "../../../fixtures/Elements.js/orderAndReturnsPageElements";
import { OrderAndReturnsPageTestData } from "../../../fixtures/TestData/orderAndReturnsPageTestData";
import { LoginPageTestData } from "../../../fixtures/TestData/loginPageTestData";

const orderAndReturnsPageElements = new OrdersAndReturnsPageElements();
const orderAndReturnsPageTestData = new OrderAndReturnsPageTestData();
const loginPageTestData = new LoginPageTestData();

export const checkOrderInformation = () => {
    cy.get(orderAndReturnsPageElements.orderIDTextbox).type(orderAndReturnsPageTestData.orderID);
    cy.get(orderAndReturnsPageElements.billingLastNameTextbox).type(orderAndReturnsPageTestData.billingLastName);
    cy.get(orderAndReturnsPageElements.emailTextbox).type(loginPageTestData.username);
    
    cy.get(orderAndReturnsPageElements.continueButton).click();
}
