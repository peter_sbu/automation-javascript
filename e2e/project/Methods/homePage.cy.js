import { HomePageElements } from "../../../fixtures/Elements.js/homePageElements";

const homePageElements = new HomePageElements();

export const gotoTopsCategory = () => {
    cy.get(homePageElements.womenDepartment).trigger('mouseover');
    cy.get(homePageElements.topsCategory).click();
}