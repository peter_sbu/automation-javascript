import { HomePageElements } from "../../../fixtures/Elements.js/homePageElements";
import { LoginPageElements } from "../../../fixtures/Elements.js/loginPageElements";

const homePageElements = new HomePageElements();
const loginPageElements = new LoginPageElements();

export const login = (username, password) => {
    cy.get(homePageElements.signInLink).first().click();
    cy.get(loginPageElements.email).type(username);
    cy.get(loginPageElements.password).type(password);
    cy.get(loginPageElements.signInButton).click();
}