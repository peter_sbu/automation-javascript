import { HomePageElements } from "../../fixtures/Elements.js/homePageElements";
import { LoginPageTestData } from "../../fixtures/TestData/loginPageTestData"
import { gotoTopsCategory } from "./Methods/homePage.cy";
import { login } from "./Methods/loginPage.cy";
import { WomenTopsPageElements } from "../../fixtures/Elements.js/womenTopsPageElements";
import { WomenTopsPageTestData } from "../../fixtures/TestData/womenTopsPageTestData";
import { showItemsInListMode, verifyAscendingOrder, confirmTotalItems} from "./Methods/womenTopsPage.cy";
import { OrdersAndReturnsPageElements } from "../../fixtures/Elements.js/orderAndReturnsPageElements";
import { OrderAndReturnsPageTestData } from "../../fixtures/TestData/orderAndReturnsPageTestData";
import { checkOrderInformation } from "./Methods/ordersAndReturnsPage.cy";

const homePageElements = new HomePageElements();
const loginPageTestData = new LoginPageTestData()
const womenTopsPageElements = new WomenTopsPageElements();
const womenTopsPageTestData = new WomenTopsPageTestData();
const orderAndReturnsPageElements = new OrdersAndReturnsPageElements();
const orderAndReturnsPageTestData = new OrderAndReturnsPageTestData();

const lumaWebsite = homePageElements.url;

describe('Navigating to LUMA website', () => {
    beforeEach(() => {
        cy.visit(lumaWebsite);
        login(loginPageTestData.username, loginPageTestData.password);
    })

    it('Navigating to the womens\' department Tops category', () => {
        cy.wait(4000);
        cy.get(homePageElements.successLogin).contains(loginPageTestData.successMessage);
        gotoTopsCategory();
        cy.get(womenTopsPageElements.pageTitle).should('contain', womenTopsPageTestData.title);
    })

    it('Confirm the displayed Tops list has a total of 25', () => { 
        gotoTopsCategory();
        cy.wait(1000);
        showItemsInListMode();
        cy.get(womenTopsPageElements.totalDisplayed).contains(womenTopsPageTestData.totalItems);
        cy.get(womenTopsPageElements.nextPage).last().click();  
        
        confirmTotalItems();
    })

    it('Sort list items by price from lowest to highest', () => {
        gotoTopsCategory();
        cy.wait(1000);
        showItemsInListMode();
      cy.wait(3000);
        cy.get(womenTopsPageElements.listSorter).first().select('Price')
          .should('have.value', womenTopsPageTestData.sortValue);

        verifyAscendingOrder();
    })

    it('Apply $40 - $49 price range and confirm that only 4 items displayed', () => {
        gotoTopsCategory();
        cy.get(womenTopsPageElements.priceRange).click();
        cy.get(womenTopsPageElements.listView).wait(1000).click();

        cy.get(womenTopsPageElements.totalDisplayed).should('contain', womenTopsPageTestData.priceRangeTotal);
    })

    it('Validate error message for the use on an invalid order id to view an order', () => {
       cy.wait(2000);
        cy.get(homePageElements.menuDropDownButton).first().click();
        cy.get(homePageElements.signInLink).first().click();
        cy.get(homePageElements.ordersAndReturns).click();

        checkOrderInformation();
        cy.get(orderAndReturnsPageElements.errorMessageElement).should('contain', orderAndReturnsPageTestData.errorMessage);
    })
})